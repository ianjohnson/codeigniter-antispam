CodeIgniter AntiSpam Library
=============================
Home: https://bitbucket.org/ianjohnson/codeigniter-antispam/overview

How to use
-----------------------------

Place the entire antispam folder into your third_party directory.
Open the antispam.php config file and set the URL you wish to use for your application.
This can either be the TypePad AntiSpam service URL or the Aksimet URL. Also you need to
add your API key for the service you plan on using. You can also change the user agent
string that will be passed to the antispam service. The library will add its version
info for you.

In your controller where you want to use the library call it like this:

	$this->load->add_package_path(APPPATH."third_party/antispam/");

	$this->load->library('antispam');
	
Then call any method with an array of as many of these optional items as possible:

	$comment = array(
		'permalink' 			=>	'', - The permanent comment location
		'comment_type' 			=>	'', - blank or comment, tracback, pingback or something made up
		'comment_author' 		=>	'', - Name submitted with the comment
		'comment_author_email'	=>	'', - Email address submitted with the comment
		'comment_author_url'	=>	'', - Submitted URL
		'comment_content'		=>	''  - the actual comment
	);
	
	$this->antispam->check_comment($comment);