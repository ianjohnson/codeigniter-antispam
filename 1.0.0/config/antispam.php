<?php

/*
|--------------------------------------------------------------------------
| Service URL
|--------------------------------------------------------------------------
|
| URL to use to connect to the antispam service. Supports both
| Akismet and TypePad AntiSpam.
|
*/
$config['service_url'] = "api.antispam.typepad.com";

/*
|--------------------------------------------------------------------------
| API Key
|--------------------------------------------------------------------------
|
| The API key from the antispam service you're using.
|
*/
$config['api_key'] = "YOUR API KEY HERE!";

/*
|--------------------------------------------------------------------------
| User Agent
|--------------------------------------------------------------------------
|
| The name and version of your application. The library will
| automatically add its own user agent ( | CodeIgniter AntiSpam v1).
|
*/
$config['user_agent'] = "MyApp/1.0";