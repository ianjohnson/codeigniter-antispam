<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

/*--------------------------------------------------------------------------------------------
|
|	Antispam Library
|
+-------------------------------------------------------------------------------------------*/

class Antispam {

	var $CI = "";

	function __construct(){
	
		$this->CI =& get_instance();
		$this->CI->config->load('antispam');
		
	}
	
	/*
	|--------------------------------------------------------------------------
	| Check Comment
	|--------------------------------------------------------------------------
	|
	| Checks the comment with the antispam service. Takes an array of optional 
	| data in this form:
	|
	| array(
	|		'permalink' 			=>	'', # The permanent comment location
	|		'comment_type' 			=>	'', # blank or comment, tracback, pingback or something made up
	|		'comment_author' 		=>	'', # Name submitted with the comment
	|		'comment_author_email'	=>	'', # Email address submitted with the comment
	|		'comment_author_url'	=>	'', # Submitted URL
	|		'comment_content'		=>	''  # the actual comment
	| );
	|
	| The more data you can supply, the more accurate the result will be.
	|
	*/
	function check_comment($data = array()){
	
		$key = $this->_build_key($data);
		
		$response = $this->_do_http_post($key, 'comment-check');
		
		if( $response == 'true' ){
		
			return TRUE;
			
		}
		
		// If you see a lot of spam, it could mean either the service is unavailable
		// or that your API is invalid
		return FALSE;
	
	}

	/*
	|--------------------------------------------------------------------------
	| Submit Ham
	|--------------------------------------------------------------------------
	|
	| Tell the service that the supplied comment is ham. Takes an array
	| of optional parameters the same as check_comment.
	|
	*/
	function submit_ham($data = array()){
	
		$key = $this->_build_key($data);
		
		$this->_do_http_post($key, 'submit-ham');
	
	}


	/*
	|--------------------------------------------------------------------------
	| Submit Spam
	|--------------------------------------------------------------------------
	|
	| Tell the service that the supplied comment is spam. Takes an array
	| of optional parameters the same as check_comment.
	|
	*/
	function submit_spam($data = array()){
	
		$key = $this->_build_key($data);
		
		$this->_do_http_post($key, 'submit-spam');
	
	}
	
	/*
	|--------------------------------------------------------------------------
	| Get API Key
	|--------------------------------------------------------------------------
	|
	| Returns the API key the library will use for the request.
	|
	*/
	function get_api_key(){
	
		return $this->CI->config->item('api_key');
		
	}
	
	/*
	|--------------------------------------------------------------------------
	| Verify Key
	|--------------------------------------------------------------------------
	|
	| Make a call to the service to check the API key in the config file
	| is valid.
	|
	*/
	function verify_key(){
	
		$this->CI->load->helper('url');
	
		$site = urlencode( site_url() );
		$key = urlencode($this->CI->config->item('api_key'));
		
		$response = $this->_do_http_post("key=$key&blog=$site", 'verify-key');
		
		if( $response == 'valid' ){
		
			return TRUE;
			
		}
		else{

			log_message('error', 'Service returned invalid API key for key ' . $this->CI->config->item('api_key'));
			return FALSE;
			
		}
	
	}
	
	/*
	|--------------------------------------------------------------------------
	| Build Key
	|--------------------------------------------------------------------------
	|
	| Builds the get request string for the API call.
	|
	*/
	private function _build_key($data = array()){
		
		$this->CI->load->helper('url');
	
		// Gather the other bits of info we need and start building a string to submit
		// This is the minimum amount of data required to make a request to these
		// services.
		$site = urlencode( site_url() );
		$ip = urlencode($_SERVER['REMOTE_ADDR']);
		$referrer = urlencode(@$_SERVER['HTTP_REFERER']);
		$useragent = urlencode($_SERVER['HTTP_USER_AGENT']);
		
		$key = "blog=$site&ip=$ip&user_agent=$useragent&referrer=$referrer";
		
		// Build request with the remaining data thats been passed in by the array
		foreach($data as $k => $v){
		
			$key = $key . "&$k=". urlencode($v);
		
		}
		
		return $key;
	
	}
	
	/*
	|--------------------------------------------------------------------------
	| Do HTTP Post
	|--------------------------------------------------------------------------
	|
	| Sends the request to the antispam service and recieves the result.
	|
	*/
	private function _do_http_post($request, $method, $port = 80) {
	
		$url = $this->CI->config->item('api_key') . "." . $this->CI->config->item('service_url');
		
		if( $method == "verify-key" ){
		
			$url = $this->CI->config->item('service_url');
		
		}
	
		$http_request  = "POST /1.1/$method HTTP/1.0\r\n";
		$http_request .= "Host: ". $url ."\r\n";
		$http_request .= "Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n";
		$http_request .= "Content-Length: " . strlen($request) . "\r\n";
		$http_request .= "User-Agent: ". $this->CI->config->item('user_agent'). " | CodeIgniter AntiSpam v1.0.0" ."\r\n";
		$http_request .= "\r\n";
		$http_request .= $request;
	
		$response = '';
		
		if( false !== ( $fs = @fsockopen($url, $port, $errno, $errstr, 3) ) ) {
		
			fwrite($fs, $http_request);
			
			while ( !feof($fs) )
				$response .= fgets($fs, 1160); // One TCP-IP packet
				
			fclose($fs);
			
			$response = explode("\r\n\r\n", $response, 2);
			
		}
		
		//var_dump($response);
		
		return @$response[1];
	}

}